function sort (str) {
    return str.toLowerCase().split(" ").map(item => {
        return item.split('').sort((val1, val2) => {
            if (val1.charCodeAt(0) === 1105) {
                if (val2.charCodeAt(0) <= 1077) {
                    return 1;
                } else {
                    return -1;
                }
            }
            if (val2.charCodeAt(0) === 1105) {
                if (val1.charCodeAt(0) > 1077) {
                    return 1;
                } else {
                    return -1;
                }
            }
            if (val1.charCodeAt(0) > val2.charCodeAt(0)) {
                return 1;
            }
            else {
                return -1;
            }
        }).join('');
    }).sort(function(val1, val2) {
        return  val1.localeCompare(val2) || val1.length - val2.length;
    }).map((val) => {
        val = val.split('');
        val[0] = val[0].toUpperCase();
        val = val.join('');
        return val;
    }).join(' ');
}

module.exports = {sort};
