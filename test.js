const operations = require('./src/assets/scripts/index.js');
const assert = require('assert');

it('1 test complete!', () => {
    assert.equal(operations.sort('Яяяяя ЯяЯ ЯЯЯя Яяяяяяяяя'), 'Яяя Яяяя Яяяяя Яяяяяяяяя');
});

it('2 test complete!', () => {
    assert.equal(operations.sort('A D E F B C'), 'A B C D E F');
});

it('3 test complete!', () => {
    assert.equal(operations.sort('Д Е А Б Г Ё Ж В'), 'А Б В Г Д Е Ё Ж');
});

it('4 test complete!', () => {
    assert.equal(operations.sort('Еиийккмоссч Абклорь Ан Еилтт Амрс'), 'Абклорь Амрс Ан Еиийккмоссч Еилтт');
});

it('5 test complete!', () => {
    assert.equal(operations.sort('Яяяяя ЯяЯ ЯЯЯя Яяяяяяяяя'), 'Яяя Яяяя Яяяяя Яяяяяяяяя');
});